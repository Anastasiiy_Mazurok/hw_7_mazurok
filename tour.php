<?php
define('USD_RATE',27.78);
$kurs=27.78;

echo define('USD_RATE',27.78) ;
$tours=[
     [
        'title'=>'Mars',
        'country'=>'Швеция',
        'price USD'=>'2000',
        'price UAH'=>'27000',
        'data'=>'25/01/2022',
    ],
    [
        'title'=>'Saturn',
        'country'=>'Бельгия',
        'price USD'=>'1000',
        'price UAH'=>'27000',
        'data'=>'26/01/2022',
    ],
    [
        'title'=>'Pluto',
        'country'=>'Исландия',
        'price USD'=>'2000',
        'price UAH'=>'56000',
        'data'=>'27/01/2022',
     ],  
     [
        'title'=>'Mercury',
        'country'=>'Финляндия',
        'price USD'=>'1000',
        'price UAH'=>'27000',
        'data'=>'28/01/2022',
     ],  
     [
        'title'=>'Uranus',
        'country'=>'Франция',
        'price USD'=>'2000',
        'price UAH'=>'56000',
        'data'=>'29/01/2022',
     ],  
     [
        'title'=>'Sun',
        'country'=>'Япония',
        'price USD'=>'2200',
        'price UAH'=>'60000',
        'data'=>'30/01/2022',
     ],  
     [
        'title'=>'Venus',
        'country'=>'Китай',
        'price USD'=>'2000',
        'price UAH'=>'56000',
        'data'=>'31/01/2022',
     ],  
     [
        'title'=>'Jupiter',
        'country'=>'ОАЭ',
        'price USD'=>'3000',
        'price UAH'=>'83000',
        'data'=>'01/02/2022',
     ],  
     [
        'title'=>'Neptune',
        'country'=>'Бразилия',
        'price USD'=>'2000',
        'price UAH'=>'56000',
        'data'=>'02/02/2022',
     ],  
     [
        'title'=>'Moon',
        'country'=>'Аляска',
        'price USD'=>'4000',
        'price UAH'=>'112000',
        'data'=>'01/03/2022',
     ],     
];

   



?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Go -Go</title>
    <meta   name="description"  content="obmen valut">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css">
</head>
<body >
    <div class="container">
        <div class="row">
        
          <div class="col-sm-6 col-lg-3"></div>
          
        </div>
    
    <div class="container">
        <!-- Content here -->
      </div>
      <h1 >Tour:</h1>
      
      <table  class="table table-striped">
      
        <tr>
            <th>#</th>
            <th>Название тура:</th>
            <th>Страна:</th>
            <th>Цена USD($)</th>
            <th>Дата</th>
            <th>Цена UAH(₴)</th>
        </tr> 
        <?php foreach($tours as $key => $tour):?>
        <tr>
        
            <td ><?=++$key?></td>
            <td ><?=$tour['title']?></td>
            <td ><?=$tour['country']?></td>
            <td ><?=$tour['price USD']?></td>
            <td ><?=$tour['data']?></td>
            <td ><?=round($tour['price USD']*$kurs,2)?></td>
            
            <?php endforeach;?>
            
        </tr>
        
       
      </table>
 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>